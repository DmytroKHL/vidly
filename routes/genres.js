const {Genre, validate} = require('../models/genre')
const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/vidly')
        .then(() => console.log('Connected to MongoDB'))
        .catch( err => console.log('MongoDB Connection error ', err))
  
  router.get('/', (req, res) => {
    getAllGenres()
        .then( result => res.send(result));
  });

    router.get('/:id', async (req, res) => {
        try {
            const genre = await Genre.findOne({ genreId: parseInt(req.params.id)})
            if(!genre) return res.status(404).send('Genre doesnt exist')
            res.send(genre)
        } catch (err)  {
            res.status(400).send(err)
        }
        
    })

  async function getAllGenres() {
    const genres = await Genre.find();
    return genres;
  }
  
  router.post('/', (req, res) => {
    const { error } = validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);
  
    const lastId = 999;
    const genre = new Genre({
        genreId: lastId + 1,
      name: req.body.name
    });
   
    genre.save()
        .then( result => res.send(result))
  });


  /// put
  router.put('/', async (req, res) => {
    // const { error } = validate(req.body); 
    // if (error) return res.status(400).send(error.details[0].message);
  
    const genre = await Genre.findOneAndUpdate( g => g.genreId === parseInt(req.body.genreId), {name: req.body.name}, {new: true})
    if(!genre) res.status(404).send('Genre not found!')
    res.send(genre)
  });

  ///delete
  router.delete('/', async (req, res) => {
  
    const genre = await Genre.findOneAndDelete({genreId: parseInt(req.body.genreId)})
    if(!genre) res.status(404).send('Genre not found!')
    res.send(genre)
  });
  
//   router.put('/:id', (req, res) => {
//     const genre = genres.find(c => c.id === parseInt(req.params.id));
//     if (!genre) return res.status(404).send('The genre with the given ID was not found.');
  
//     const { error } = validate(req.body); 
//     if (error) return res.status(400).send(error.details[0].message);
    
//     genre.name = req.body.name; 
//     res.send(genre);
//   });
  
//   router.delete('/:id', (req, res) => {
//     const genre = genres.find(c => c.id === parseInt(req.params.id));
//     if (!genre) return res.status(404).send('The genre with the given ID was not found.');
  
//     const index = genres.indexOf(genre);
//     genres.splice(index, 1);
  
//     res.send(genre);
//   });
  
//   router.get('/:id', (req, res) => {
//     const genre = genres.find(c => c.id === parseInt(req.params.id));
//     if (!genre) return res.status(404).send('The genre with the given ID was not found.');
//     res.send(genre);
//   });
  

module.exports = router
  