const Joi = require('joi');
const mongoose = require('mongoose')

const genreSchema = new mongoose.Schema({
    genreId: Number,
    name: String
})

const Genre = mongoose.model('Genre', genreSchema)

function validateGenre(genre) {
    const schema =  Joi.object({
            name: Joi.string()
                    .min(3)
                    .required()
    });
  
    console.log(genre)
    return schema.validate(genre);
}

exports.Genre = Genre
exports.validate = validateGenre()